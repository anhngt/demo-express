const Category = require("../Models/Category");

module.exports = {
    create: function (req, res, next) {
        res.render('pages/category/form', { title: 'Express' });
    },
    store: function (req, res, next) {
        var category = new Category({
            title: req.body.title
        })
        category.save(function (err) {
            if (err) {
                console.log('Lỗi' + err);
            } else {
                console.log('Lưu thành công');
            }
        })
        res.redirect('/category');
    },
    update: function (req, res) {
        // console.log(req.body);
        var id = req.body._id;
        var title = req.body.title;
        Category.findOneAndUpdate({ _id: id }, req.body, { new: true }, function (err, body) {
            if (err) {
                console.log('lỗi:' + err);
            } else {
                res.redirect('/category');
            }
        });
    },
    index: function (req, res) {
        // .skip(0).limit(2)
        Category.find()
            .exec()
            .then(category => {
                res.render('pages/category/index', { category: category })
            });


    },
    edit: function (req, res) {
        Category.findOne({ _id: req.params.id })
            .exec()
            .then(category => {
                res.render('pages/category/form', { category })
            });
    },
    delete: function (req, res) {
        Category.remove({ _id: req.params.id }, function (err) {
            if (!err) {
                res.redirect('/category');
            } else {
                console.log('Lỗi' + err);
            }
        });
    }
}
"use strict";

var Category = require("../Models/Category");

module.exports = {
  create: function create(req, res, next) {
    res.render('pages/category/form', {
      title: 'Express'
    });
  },
  store: function store(req, res, next) {
    var category = new Category({
      title: req.body.title
    });
    category.save(function (err) {
      if (err) {
        console.log('Lỗi' + err);
      } else {
        console.log('Lưu thành công');
      }
    });
    res.redirect('/category');
  },
  update: function update(req, res) {
    // console.log(req.body);
    var id = req.body._id;
    var title = req.body.title;
    Category.findOneAndUpdate({
      _id: id
    }, req.body, {
      "new": true
    }, function (err, body) {
      if (err) {
        console.log('lỗi:' + err);
      } else {
        res.redirect('/category');
      }
    });
  },
  index: function index(req, res) {
    // .skip(0).limit(2)
    Category.find().exec().then(function (category) {
      res.render('pages/category/index', {
        category: category
      });
    });
  },
  edit: function edit(req, res) {
    Category.findOne({
      _id: req.params.id
    }).exec().then(function (category) {
      res.render('pages/category/form', {
        category: category
      });
    });
  },
  "delete": function _delete(req, res) {
    Category.remove({
      _id: req.params.id
    }, function (err) {
      if (!err) {
        res.redirect('/category');
      } else {
        console.log('Lỗi' + err);
      }
    });
  }
};
//# sourceMappingURL=categoryController.dev.js.map

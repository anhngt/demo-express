var express = require('express');
var router = express.Router();
var categoriesRouter = require('./categories');
/* GET home page. */
router.get('/', function (req, res, next) {
    res.render('layouts/master', { title: 'Express' });
});

router.get('/dashboard', function (req, res, next) {
    res.render('pages/dashboard/index', { title: 'Express' });
});

router.use('/category', categoriesRouter)
module.exports = router;
